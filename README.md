# Install


### install gulp global

_npm install gulp -g_

### install node modules

_npm install_

**_не исправлять конфликты!_**

# Tasks

### create js css

_gulp create_

### create js.min css.min

_gulp create_min_

### img minification

_gulp img_min_

### svg copy

_gulp svg_copy_

### fonts copy

_gulp font_copy_

### watcher and browser sync

_gulp watch_
