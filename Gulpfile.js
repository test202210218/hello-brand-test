const gulp = require('gulp'),
    imagemin = require('gulp-imagemin');
    webp = require('gulp-webp');
    sass = require('gulp-sass'),
    sourceMaps = require('gulp-sourcemaps'),
    watch = require('gulp-watch'),
    autoPrefix = require('gulp-autoprefixer'),
    browserSync = require('browser-sync'),
    concat = require('gulp-concat'),
    cleanCss = require('gulp-clean-css'),
    uglify = require('gulp-uglify-es').default,
    baseUrl = './';

function jpg_webp() {
    return gulp.src('./source/**/*.jpg')
        .pipe(imagemin())
        .pipe(webp())
        .pipe(gulp.dest(baseUrl))
}

function img_min() {
    return gulp.src('./source/**/*.+(jpg|png)')
        .pipe(imagemin())
        .pipe(gulp.dest(baseUrl))
}

function svg_copy() {
    return gulp.src('./source/**/*.svg')
        .pipe(gulp.dest(baseUrl))
}

function font_copy() {
    return gulp.src('./source/**/*.+(woff|woff2)')
        .pipe(gulp.dest(baseUrl))
}

function create_css() {
    return gulp.src('./source/**/*.+(sass|scss)')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('style.css'))
        .pipe(autoPrefix())
        .pipe(gulp.dest(baseUrl + 'css/'));
}

function create_min_css() {
    return gulp.src('./source/**/*.+(sass|scss)')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(concat('style.min.css'))
        .pipe(autoPrefix())
        .pipe(sourceMaps.init())
        .pipe(sourceMaps.write('./'))
        .pipe(cleanCss())
        .pipe(gulp.dest(baseUrl + 'css/'));
}

function create_js() {
    return gulp.src('./source/**/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest(baseUrl + '/js/'));
}

function create_min_js() {
    return gulp.src('./source/**/*.js')
        .pipe(concat('main.min.js'))
        .pipe(sourceMaps.init())
        .pipe(uglify())
        .pipe(sourceMaps.write('./'))
        .pipe(gulp.dest(baseUrl + 'js/'));
}

function brSync() {
    return browserSync.init({
        server: {
            baseDir: "./"
        },
        port: 3000,
        notify: false
    });
}

function watch_files() {
    gulp.watch('./*.html').on('change', browserSync.reload);
    gulp.watch(['./source/**/*.+(sass|scss)'], gulp.parallel(create_css, create_min_css)).on('change', browserSync.reload);
    gulp.watch(['./source/**/*.js'], gulp.parallel(create_js, create_min_js)).on('change', browserSync.reload);
    gulp.watch(['./source/**/*.+(jpg|png)'], img_min).on('change', browserSync.reload);
    gulp.watch(['./source/**/*.jpg'], jpg_webp).on('change', browserSync.reload);
    gulp.watch(['./source/**/*.svg'], svg_copy).on('change', browserSync.reload);
}

gulp.task('create', gulp.parallel(create_css, create_js));
gulp.task('create_min', gulp.parallel(create_min_css, create_min_js));
gulp.task('img_min', gulp.parallel(img_min, jpg_webp));
gulp.task('svg_copy', svg_copy);
gulp.task('font_copy', font_copy);
gulp.task('watch', gulp.parallel(brSync, watch_files));
