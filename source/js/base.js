window.onload = function () {
  new Swiper('.cite__slider', {
    loop: true,
    centeredSlides: true,
    spaceBetween: 40,
    pagination: {
      el: '.swiper-pagination',
    },
    navigation: {
      nextEl: '.cite__slider-next',
      prevEl: '.cite__slider-prev',
    }
  })

  new Swiper('.gallery__slider', {
    loop: true,
    centeredSlides: true,
    spaceBetween: 20,
    breakpoints: {
      768: {
        spaceBetween: 30,
        slidesPerView: 1.5,
      },
      992: {
        spaceBetween: 30,
        slidesPerView: 2,
      }
    },
    pagination: {
      el: '.swiper-pagination',
    },
    navigation: {
      nextEl: '.gallery__slider-next',
      prevEl: '.gallery__slider-prev',
    }
  })

  const menu = document.querySelector('[data-role="menu"]')
  const sandwich = document.querySelector('[data-role="toggle-menu"]')

  sandwich.addEventListener('click', function (e) {
    e.preventDefault()
    sandwich.classList.toggle('active')
    if (sandwich.classList.contains('active')) {
      menu.classList.add('show')
    } else {
      menu.classList.remove('show')
    }
  }, false)

  window.onscroll = function(){
    sandwich.classList.remove('active')
    menu.classList.remove('show')
  }

  const modalWrapper = document.querySelector('[data-role="modal-wrapper"]')
  const modal = document.querySelector('[data-role="modal"]')
  const showModal = document.querySelector('[data-role="show-modal"]')

  showModal.addEventListener('click', function (e) {
    e.preventDefault()
    document.body.style.overflow = 'hidden'

    modalWrapper.hidden = false
    modalWrapper.classList.add('show')
  }, false)

  modalWrapper.addEventListener('click', function () {
    console.log('Данные формы отправлены')
    document.body.style.overflow = 'auto'
    modalWrapper.classList.remove('show')
    modalWrapper.hidden = true
  }, false)

  modal.addEventListener('click', function (e) {
    e.stopPropagation()
  }, false)

  const forms = document.querySelectorAll('form')

  for (let i = 0; i < forms.length; i++) {
    forms[i].addEventListener('submit', function (e) {
      e.preventDefault()

      const body = {
        name: this.querySelector('[name="name"]').value,
        email: this.querySelector('[name="email"]').value,
        request: this.querySelector('[name="request"]').value,
      }
      const request = new XMLHttpRequest()

      request.addEventListener('load', function(){
        document.body.style.overflow = 'auto'
        modalWrapper.classList.remove('show')
        modalWrapper.hidden = true
      })
      request.open('POST', '/send.php', true)
      request.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8')
      request.send('name=' + encodeURIComponent(body.name) + '&email=' + encodeURIComponent(body.email) + '&request=' + encodeURIComponent(body.request))
    })
  }

  console.log('Loaded')
}
